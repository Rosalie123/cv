Rails.application.routes.draw do

  root 'cv_pages#home'
  get '/cv', to: 'cv_pages#show'
  get '/about', to: 'cv_pages#about'
  get '/portfolio', to:'cv_pages#portfolio'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
